# recipe-app-api-proxy

NGINX proxy for Recipe App API

## Usage

## Env Variables

* `Listen Port` - Port to listen on (Default - 8000)
* `APP_HOST` - Hostname of the app to forward req to (default - 'app')
* `APP_PORT` - port to forward request to - (Default - 9000)

